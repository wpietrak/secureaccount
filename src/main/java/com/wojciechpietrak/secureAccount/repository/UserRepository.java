package com.wojciechpietrak.secureAccount.repository;

import com.wojciechpietrak.secureAccount.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository <User, Long> {
}
