package com.wojciechpietrak.secureAccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecureAccountApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecureAccountApplication.class, args);
	}
}
